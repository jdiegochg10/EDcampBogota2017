<?php

namespace App\Http\Controllers;

use App\Models\Invoice;
use App\Models\Price;
use App\Models\Subscription;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use PayU;
use SupportedLanguages;
use Environment;
use PayUParameters;
use PayUPayments;
use PayUCountries;

class SubscriptionsController extends Controller
{
    public function subscribe()
    {
        $price = Price::orderBy('actual', 'desc')->orderBy('ends_at', 'desc')->first();

        return view('users.subscription', compact('price'));
    }

    public function payu(Request $request)
    {
        PayU::$apiKey = "4Vj8eK4rloUd272L48hsrarnUA"; //Ingrese aquí su propio apiKey.
        PayU::$apiLogin = "pRRXKOl8ikMmt9u"; //Ingrese aquí su propio apiLogin.
        PayU::$merchantId = "508029"; //Ingrese aquí su Id de Comercio.
        PayU::$language = SupportedLanguages::ES; //Seleccione el idioma.
        PayU::$isTest = true; //Dejarlo True cuando sean pruebas.

        // URL de Pagos
        Environment::setPaymentsCustomUrl("https://sandbox.api.payulatam.com/payments-api/4.0/service.cgi");

        // URL de Consultas
        Environment::setReportsCustomUrl("https://sandbox.api.payulatam.com/reports-api/4.0/service.cgi");

        // URL de Suscripciones para Pagos Recurrentes
        Environment::setSubscriptionsCustomUrl("https://sandbox.api.payulatam.com/payments-api/rest/v4.3/");

        // Código único de la transacción
        $reference = 'ed_' . auth()->user()->id . date('_Y-m-d-H-i-s');

        // Precio más reciente
        $price = Price::orderBy('actual', 'desc')->orderBy('ends_at', 'desc')->first();

        // El valor que se cobrará al usuario
        $total_value = $price->price * $request->get('months');

        //para realizar un pago con tarjeta de crédito
        $parameters = array(
            //Ingrese aquí el identificador de la cuenta.
            // Colombia: 512321
            PayUParameters::ACCOUNT_ID => "512321",
            //Ingrese aquí el código de referencia.
            PayUParameters::REFERENCE_CODE => $reference,
            //Ingrese aquí la descripción.
            PayUParameters::DESCRIPTION => "payment test para suscripcion",

            // -- Valores --
            //Ingrese aquí el valor.
            PayUParameters::VALUE => $total_value,
            //Ingrese aquí la moneda.
            PayUParameters::CURRENCY => "COP",

            // -- Comprador
            //Ingrese aquí el nombre del comprador.
            PayUParameters::BUYER_NAME => auth()->user()->name,
            //Ingrese aquí el email del comprador.
            PayUParameters::BUYER_EMAIL => auth()->user()->email,
            //Ingrese aquí el teléfono de contacto del comprador.
            PayUParameters::BUYER_CONTACT_PHONE => "7563126",
            //Ingrese aquí el documento de contacto del comprador.
            PayUParameters::BUYER_DNI => "5415668464654",
            //Ingrese aquí la dirección del comprador.
            PayUParameters::BUYER_STREET => "calle 100",
            PayUParameters::BUYER_STREET_2 => "5555487",
            PayUParameters::BUYER_CITY => "Medellin",
            PayUParameters::BUYER_STATE => "Antioquia",
            PayUParameters::BUYER_COUNTRY => "CO",
            PayUParameters::BUYER_POSTAL_CODE => "000000",
            PayUParameters::BUYER_PHONE => "7563126",

            // -- pagador --
            //Ingrese aquí el nombre del pagador.
            PayUParameters::PAYER_NAME => "APPROVED",
            //Ingrese aquí el email del pagador.
            PayUParameters::PAYER_EMAIL => "edstyles-colombia@outlook.com",
            //Ingrese aquí el teléfono de contacto del pagador.
            PayUParameters::PAYER_CONTACT_PHONE => "7563126",
            //Ingrese aquí el documento de contacto del pagador.
            PayUParameters::PAYER_DNI => "5415668464654",
            //Ingrese aquí la dirección del pagador.
            PayUParameters::PAYER_STREET => "calle 93",
            PayUParameters::PAYER_STREET_2 => "125544",
            PayUParameters::PAYER_CITY => "Bogota",
            PayUParameters::PAYER_STATE => "Bogota",
            PayUParameters::PAYER_COUNTRY => "CO",
            PayUParameters::PAYER_POSTAL_CODE => "000000",
            PayUParameters::PAYER_PHONE => "7563126",

            // -- Datos de la tarjeta de crédito --
            //Ingrese aquí el número de la tarjeta de crédito
            PayUParameters::CREDIT_CARD_NUMBER => "4097440000000004",
            //Ingrese aquí la fecha de vencimiento de la tarjeta de crédito
            PayUParameters::CREDIT_CARD_EXPIRATION_DATE => "2017/12",
            //Ingrese aquí el código de seguridad de la tarjeta de crédito
            PayUParameters::CREDIT_CARD_SECURITY_CODE=> "321",
            //Ingrese aquí el nombre de la tarjeta de crédito
            //VISA||MASTERCARD||AMEX||DINERS
            PayUParameters::PAYMENT_METHOD => "VISA",

            //Ingrese aquí el número de cuotas.
            PayUParameters::INSTALLMENTS_NUMBER => "1",
            //Ingrese aquí el nombre del pais.
            PayUParameters::COUNTRY => PayUCountries::CO,

            //Session id del device.
            PayUParameters::DEVICE_SESSION_ID => "vghs6tvkcle931686k1900o6e1",
            //IP del pagadador
            PayUParameters::IP_ADDRESS => "127.0.0.1",
            //Cookie de la sesión actual.
            PayUParameters::PAYER_COOKIE=>"pt1t38347bs6jc9ruv2ecpv7o2",
            //Cookie de la sesión actual.
            PayUParameters::USER_AGENT=>"Mozilla/5.0 (Windows NT 5.1; rv:18.0) Gecko/20100101 Firefox/18.0"
        );

        //solicitud de autorización y captura
        $response = PayUPayments::doAuthorizationAndCapture($parameters);

        // Podrás obtener las propiedades de la respuesta
        if($response){
            // Inprime información y detiene el proceso.
            //dd($response);

            //$response->transactionResponse->orderId;
            //$response->transactionResponse->transactionId;
            //$response->transactionResponse->state;
            if(in_array($response->transactionResponse->state, ['PENDING', 'DECLINED'])) {
                $status = 'Transacción PayU '
                    . $response->transactionResponse->transactionId
                    . " "
                    . $response->transactionResponse->state
                    . ": "
                    . $response->transactionResponse->responseCode;

                Log::alert($status);

                return view('users.subscription', compact('status', 'price'));
            }
            //$response->transactionResponse->paymentNetworkResponseCode;
            //$response->transactionResponse->paymentNetworkResponseErrorMessage;
            //$response->transactionResponse->trazabilityCode;
            //$response->transactionResponse->responseCode;
            //$response->transactionResponse->responseMessage;

            $status = 'Transacción PayU '
                . $response->transactionResponse->transactionId
                . " exitosa "
                . $response->transactionResponse->state
                . ": "
                . $response->transactionResponse->responseCode;

            Log::info($status);

            if ($response->transactionResponse->state == 'APPROVED') {
                // Crea el registro en la tabla invoices
                $invoice = new Invoice();
                $invoice->invoice_date = Carbon::now();
                $invoice->user_id = auth()->user()->id;
                $invoice->state = 'APPROVED';
                $invoice->price = $total_value;
                $invoice->save();

                // Activa la suscripción del usuario
                $subscription = new Subscription();
                $subscription->invoice_id = $invoice->id;
                $subscription->user_id = $invoice->user_id;
                $subscription->begins_at = Carbon::now();
                $subscription->ends_at = Carbon::now()->addMonths($request->get('months'));
                $subscription->actual = true;
                $subscription->save();

                // Actualiza los datos de la sessión de usuario
                auth()->user()->state = 'ACTIVE';
                auth()->user()->save();
            }

            return redirect()->route('users.profile');
        }
    }
}
