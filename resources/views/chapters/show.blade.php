@extends('layouts.master')

@section('header')
    @include('partials.header')
    @include('partials.user_menu')
@endsection

@section('content')
    <main class="Chapters  u-container  u-afterFixed">
        <header class="u-title">
            <h2>{{ $chapter->name }}</h2>
        </header>
        <p class="u-data">
            Serie {{ $chapter->serie->name }}.
        </p>
        <p class="u-data">
            {{ $chapter->description }}
        </p>
        <p class="u-data">
            Duración: {{ $chapter->duration }}
        </p>
        <div class="u-fullScreen">
            {!! $chapter->chapter_url !!}
        </div>

        @include('series.partials.list', ['serie' => $chapter->serie])
    </main>
@endsection
