@extends('layouts.master')

@section('header')
    @include('partials.header')
    @include('partials.user_menu')
@endsection

@section('content')
    <main class="Series  u-container  u-afterFixed">
        <header class="u-title">
            <h2>{{ $serie->name }}</h2>
        </header>
        {!! Form::model(
            $serie,
            [
                'route' => ['admin.series.update', $serie],
                'files' => 'true',
                'method' => 'PUT'
            ]
        ) !!}
            @include('series.partials.form')
        {!! Form::close() !!}

        @if($serie->picture)
            <div class="u-container">
                <h2>Imagen actual</h2>
                <img src="{{ Storage::url($serie->picture) }}" alt="{{ $serie->name }}">
            </div>
        @endif
        @if($serie->trailer_url)
            <br>
            <div class="u-container">
                <h2>Video actual</h2>
                <div>
                    {{!! $serie->trailer_url !!}}
                </div>
            </div>
        @endif
    </main>
@endsection
